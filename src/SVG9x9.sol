// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.0;

import "./PixelCloud.sol";
import "./StringUtils.sol";

/**
 * @dev 9x9 square of random pixels in SVG format.
 */
library SVG9x9 {
    /**
     * @dev Render SVG of 9x9.
     * @param _content Inner SVG content
     * @return SVG markup.
     */
    function renderImage(string memory _content) internal pure returns (string memory) {
        return string(abi.encodePacked(
            "<svg xmlns=\\\"http://www.w3.org/2000/svg\\\" style=\\\"width: 100%; height: 100%\\\" viewBox=\\\"0 0 9 9\\\">",
            _content,
            "</svg>"
        ));
    }
    
    /**
     * @dev Generate deterministic pixel could as SVG image from string used as a seed.
     * @param _name Domain name or other string to make the cloud.
     * @return pixel cloud SVG image.
     */
    function renderPixelCloud(string memory _name) internal pure returns (string memory) {
        return renderImage(string(abi.encodePacked(
            renderPixelCloudNoWrap(_name),
            renderText(_name)
        )));
    }

    /**
     * @dev Generate deterministic pixel could in form of SVG rectangle notations from string used as a seed.
     * @param _name Domain name or other string to make the cloud.
     * @return SVG group of pixel cloud rectangles.
     */
    function renderPixelCloudNoWrap(string memory _name) internal pure returns (string memory) {
        bytes32 _chunk1 = keccak256(bytes(_name));
        bytes32 _chunk2 = keccak256(abi.encodePacked(_chunk1));

        bytes memory _rendered;

        for (uint256 _i = 0; _i < 81; _i++) {
            _rendered = abi.encodePacked(_rendered, PixelCloud.renderBlock(PixelCloud.PixelBlock(
                _i % 9,
                _i / 9,
                1,
                1,
                PixelCloud.getColor(_i, _chunk1, _chunk2)
            )));
        }

        return string(abi.encodePacked(
            "<g>",
            _rendered,
            "</g>"
        ));
    }

    /**
     * @dev SVG markup for text.
     * @param _text Arbitary text.
     * @return text SVG node notation.
     */
    function renderText(string memory _text) internal pure returns (string memory) {
        uint256 _len = StringUtils.strlen(_text);
        if (_len > 11) {
            return string(abi.encodePacked(
                "<text fill=\\\"#ffffff\\\" x=\\\"0.5\\\" y=\\\"1.5\\\" style=\\\"font: bold 1pt 'Roboto sans-serif'\\\">",
                StringUtils.substring(_text, 0, 11),
                "</text>",
                "<text fill=\\\"#ffffff\\\" x=\\\"0.5\\\" y=\\\"3\\\" style=\\\"font: bold 1pt 'Roboto sans-serif'\\\">",
                StringUtils.substring(_text, 11, _len),
                "</text>"
            ));
        } else {
            return string(abi.encodePacked(
                "<text fill=\\\"#ffffff\\\" x=\\\"0.5\\\" y=\\\"1.5\\\" style=\\\"font: bold 1pt 'Roboto sans-serif'\\\">",
                _text,
                "</text>"
            ));
        }
    }
}
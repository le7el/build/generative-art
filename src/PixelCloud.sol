// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.0;

/**
 * @dev Random selector of colors (16 bit palette) to generate pixel clouds.
 */
library PixelCloud {
    uint24 constant public PALETTE_0 = 0x9803d6;
    uint24 constant public PALETTE_1 = 0xd65efe;
    uint24 constant public PALETTE_2 = 0x1369d3;
    uint24 constant public PALETTE_3 = 0x00b4ff;
    uint24 constant public PALETTE_4 = 0x5eb5fe;
    uint24 constant public PALETTE_5 = 0x27d0e9;
    uint24 constant public PALETTE_6 = 0x5ee9fe;
    uint24 constant public PALETTE_7 = 0x2fbadd;
    uint24 constant public PALETTE_8 = 0xe9409b;
    uint24 constant public PALETTE_9 = 0xfe5ec4;
    uint24 constant public PALETTE_A = 0xec2cea;
    uint24 constant public PALETTE_B = 0xec2cc6;
    uint24 constant public PALETTE_C = 0xdf47c3;
    uint24 constant public PALETTE_D = 0xee66b0;
    uint24 constant public PALETTE_E = 0xd65efe;
    uint24 constant public PALETTE_F = 0xfe5ec4;

    struct PixelBlock {
        uint256 x;
        uint256 y;
        uint256 w;
        uint256 h;  
        uint24 color;    
    }

    /**
     * @dev Define color based on hex digit.
     * @param _i index from 0 to 63.
     * @param _chunk1 bytes32 chunk used as storage.
     * @param _chunk2 additional bytes32 chunk used as storage for indexes between 64 and 127.
     * @return color as hex number.
     */
    function getColor(uint256 _i, bytes32 _chunk1, bytes32 _chunk2) internal pure returns (uint24) {
        require(_i < 127, "index overflow");
        if (_i < 64) {
            return getColorByChar(getCharByIndex(_i, _chunk1));
        } else {
            return getColorByChar(getCharByIndex(_i % 64, _chunk2));
        }
    }

    /**
     * @dev Use bytes32 storage as 64 index storage by utilising hex encoding.
     * @param _i index from 0 to 63.
     * @param _chunk bytes32 chunk used as storage.
     * @return single character byte in hex encoding.
     */
    function getCharByIndex(uint256 _i, bytes32 _chunk) internal pure returns (bytes1) {
        require(_i < 64, "index overflow");
        uint256 _j = _i / 2;
        uint256 _m = _i % 2;

        bytes1 _currentByte = bytes1(_chunk << (_j * 8));
        
        uint8 _c1 = uint8(bytes1((_currentByte << 4) >> 4));
        uint8 _c2 = uint8(bytes1((_currentByte >> 4)));
    
        if (_m == 0) {
            if (_c2 >= 0 && _c2 <= 9) return bytes1(_c2 + 48);
            return bytes1(_c2 + 87);
        }

        if (_c1 >= 0 && _c1 <= 9) return bytes1(_c1 + 48);
        return bytes1(_c1 + 87);
    }

    /**
     * @dev Define color based on hex digit.
     * @param _char digit byte in hex encoding.
     * @return hex number of a color to optimise storage.
     */
    function getColorByChar(bytes1 _char) internal pure returns (uint24) {
        if (_char == bytes1("1")) return PALETTE_1;
        if (_char == bytes1("2")) return PALETTE_2;
        if (_char == bytes1("3")) return PALETTE_3;
        if (_char == bytes1("4")) return PALETTE_4;
        if (_char == bytes1("5")) return PALETTE_5;
        if (_char == bytes1("6")) return PALETTE_6;
        if (_char == bytes1("7")) return PALETTE_7;
        if (_char == bytes1("8")) return PALETTE_8;
        if (_char == bytes1("9")) return PALETTE_9;
        if (_char == bytes1("a")) return PALETTE_A;
        if (_char == bytes1("b")) return PALETTE_B;
        if (_char == bytes1("c")) return PALETTE_C;
        if (_char == bytes1("d")) return PALETTE_D;
        if (_char == bytes1("e")) return PALETTE_E;
        if (_char == bytes1("f")) return PALETTE_F;
        return PALETTE_0;
    }

    /**
     * @dev Render individual rectangle in a pixel cloud based on it's index.
     * @param _block pixel structure for rendering.
     * @return SVG rectangle string as utf8 bytes.
     */
    function renderBlock(PixelBlock memory _block) internal pure returns (bytes memory) {
        return abi.encodePacked(
            "<rect x=\\\"", toDecString(_block.x),
            "\\\" y=\\\"", toDecString(_block.y), "\\\" width=\\\"", toDecString(_block.w),
            "\\\" height=\\\"", toDecString(_block.h),
            "\\\" fill=\\\"#", toHexColor(toHexString(_block.color)), "\\\" />"
        );
    }

    /**
     * @dev Convert individual digit in a hex form.
     * @param _d hex color number for conversion
     * @return single digit byte in a hex encoding
     */
    function toHexDigit(uint8 _d) internal pure returns (bytes1) {
        if (0 <= _d && _d <= 9) {
            return bytes1(uint8(bytes1("0")) + _d);
        } else if (10 <= uint8(_d) && uint8(_d) <= 15) {
            return bytes1(uint8(bytes1("a")) + _d - 10);
        }
        revert("Invalid hex digit");
    }

    /**
     * @dev Convert number to string in a hex form e.g. 0xff to '0xff'
     * @param _a hex color number for conversion
     * @return string in a hex encoding
     */
    function toHexString(uint256 _a) internal pure returns (string memory) {
        uint256 _count = 0;
        uint256 _b = _a;
        while (_b != 0) {
            _count++;
            _b /= 16;
        }
        bytes memory _res = new bytes(_count);
        for (uint256 _i = 0; _i < _count; ++_i) {
            _b = _a % 16;
            _res[_count - _i - 1] = toHexDigit(uint8(_b));
            _a /= 16;
        }
        return string(_res);
    }

    /**
     * @dev Conversion to string may trim the leading 00 for the hex color, ensure that it's there.
     * @param _str string to check.
     * @return valid color string in a hex encoding.
     */
    function toHexColor(string memory _str) internal pure returns (string memory) {
        if (bytes(_str).length < 6) return string(abi.encodePacked("00", _str));
        return _str;
    }

    /**
     * @dev oraclizeAPI function to convert uint256 to memory string.
     * @param _i number to convert.
     * @return number string in a decimal encoding.
     */
    function toDecString(uint256 _i) internal pure returns (string memory) {
        if (_i == 0) {
            return "0";
        }
        uint256 j = _i;
        uint256 len;
        while (j != 0) {
            len++;
            j /= 10;
        }
        bytes memory bstr = new bytes(len);
        uint256 k = len - 1;
        
        unchecked {
            while (_i != 0) {
                bstr[k--] = bytes1(uint8(48 + _i % 10));
                _i /= 10;
            }
        }
        return string(bstr);
    }
}

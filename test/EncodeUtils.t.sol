// SPDX-License-Identifier: MPL-2.0

pragma solidity ^0.8.10;

import "forge-std/Test.sol";
import "src/EncodeUtils.sol";

contract EncodeUtilsTest is Test {
    // using EncodeUtils for string;

    function setUp() public {
    }

    function testNFTAttributeJSONGeneration() public {
        assertTrue(
            keccak256(abi.encodePacked(EncodeUtils.attributeForTypeAndValue("image", "ipfs://QmTbFbJAGfPJ33sVmTJSoPrKxRjXyTbz1RuQ37ijpPX1LZ", false))) ==
            keccak256(abi.encodePacked("{\"trait_type\":\"image\",\"value\":\"ipfs://QmTbFbJAGfPJ33sVmTJSoPrKxRjXyTbz1RuQ37ijpPX1LZ\"}"))
        );
    }

    function testNFTNumericAttributeJSONGeneration() public {
        assertTrue(
            keccak256(abi.encodePacked(EncodeUtils.attributeForTypeAndValue("strAttribute", "903", true))) ==
            keccak256(abi.encodePacked("{\"trait_type\":\"strAttribute\",\"value\":903}"))
        );
    }

    function testBase64Encoding() public {
        assertTrue(
            keccak256(abi.encodePacked(EncodeUtils.base64("some\\r\\n Weird33!teXt~"))) ==
            keccak256(abi.encodePacked("c29tZVxyXG4gV2VpcmQzMyF0ZVh0fg=="))
        );
    }

    function testBase64EmptyEncoding() public {
        assertTrue(
            keccak256(abi.encodePacked(EncodeUtils.base64(""))) ==
            keccak256(abi.encodePacked(""))
        );
    }

    function testBase64ZeroEncoding() public {
        assertTrue(
            keccak256(abi.encodePacked(EncodeUtils.base64("0"))) ==
            keccak256(abi.encodePacked("MA=="))
        );
    }
}

// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.10;

import "forge-std/Script.sol";

import {PixelCloud} from "src/PixelCloud.sol";

contract PixelCloudScript is Script {
    using PixelCloud for string;

    function setUp() public {}

    function run() public {
        vm.broadcast();
    }
}

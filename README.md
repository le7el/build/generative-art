# Generative art

Set of Solidity libraries to create on-chain graphics.

# Getting started

Install [Foundry](https://getfoundry.sh/).

Run tests: `forge test`

## Installation

    npm i @le7el/generative_art

# API

## SVG9x9

### renderPixelCloud(string memory seed)

Deterministically generates an SVG image with pixel as a rectangle of 9x9 pixels colored by random colors.

### renderPixelCloudNoWrap(string memory seed)

Deterministically generates SVG rectangle notation (without `<svg>` enclosement) for pixel cloud of 9x9 pixels colored by random colors.

### renderText(string memory seed)

Render white colored text for 9x9 SVG canvas.

### renderImage(string memory innerContent)

Wrap inner SVG notations with `<svg>` wrapper tag.

## EncodeUtils

### base64(bytes memory data)

Base64 encoding, keep in mind that `\` required double escaping like `\\r\\n`.

### attributeForTypeAndValue(string memory traitType, string memory value, bool isNumber)

JSON encode attribute and value into `{"trait_type":"[traitType]","value":"[value]"}` dictionary record.

### attributeNameAndValue(string memory name, string memory value, bool isNumber, bool isLast)

JSON encode metadata chunk as `"name":"value",` for further JSON rendering.

## StringUtils

### strlen(string memory str)

Get a byte length of a string.

### substring(string memory str, uint startIndex, uint endIndex)

Get a substring between `startIndex` and `endIndex`, excluding the `endIndex` character.